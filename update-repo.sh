#!/usr/bin/env bash

# Desc: This shell script allows to update the repository database
#
# Copyright (C) 2021-2022 Arcigo Linux <arcigo.linux@gmail.com>
# Everyone is permitted to copy and distribute copies of this file under GNU-GPL3
#

total_packages=$(ls x86_64 | grep 'zst$' | wc -l)
repo_name="arcigo-repo"

_green_text(){
	QUERY="$*"
	echo -e "\e[32m${QUERY}\e[0m"
}

_red_text(){
	QUERY="$*"
	echo -e "\e[31m${QUERY}\e[0m"
}

_green_text "\n[+] Total packages in repository: ${total_packages}"
sleep 2

cd x86_64/

if [ -f ${repo_name}.db ]; then
    _green_text "\n==> Trying to update packages and database..."
    _green_text "\n==> Removing the repository files and database\n"
    rm -rf ${repo_name}.*
    _green_text "==> Updating the repository\n"
    sleep 2
    # Create a package database
    repo-add -n -R arcigo-repo.db.tar.gz *.tar.zst

	if [[ $? == 0 ]]; then
	    rm ${repo_name}.db
	    # rm ${repo_name}.db.sig

	    rm ${repo_name}.files
	    # rm ${repo_name}.files.sig

	    mv ${repo_name}.db.tar.gz ${repo_name}.db
	    # mv ${repo_name}.db.tar.gz.sig ${repo_name}.db.sig

	    mv ${repo_name}.files.tar.gz ${repo_name}.files
	    # mv ${repo_name}.files.tar.gz.sig ${repo_name}.files.sig
        
        _green_text "\n==> Repository Updated\n"
	else
	    _red_text "\n==> An error occured in updating repository.\n"
	    exit 1
	fi
else
	_green_text "\n==> Creating the repository\n"
	sleep 2
    # Create a package database
    repo-add -n -R arcigo-repo.db.tar.gz *.tar.zst

	if [[ $? == 0 ]]; then
	    rm ${repo_name}.db
	    # rm ${repo_name}.db.sig

	    rm ${repo_name}.files
	    # rm ${repo_name}.files.sig

	    mv ${repo_name}.db.tar.gz ${repo_name}.db
	    # mv ${repo_name}.db.tar.gz.sig ${repo_name}.db.sig

	    mv ${repo_name}.files.tar.gz ${repo_name}.files
	    # mv ${repo_name}.files.tar.gz.sig ${repo_name}.files.sig
        _green_text "\n==> Repository Updated\n"
	else
	    _red_text "\n==> An error occured in updating repository.\n"
	    exit 1
	fi
fi
